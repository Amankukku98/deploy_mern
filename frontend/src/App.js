import React,{useEffect} from 'react'
import axios from 'axios';
function App() {
  useEffect(()=>{
    axios.get('https://deploy-mern-three.vercel.app/data').then((response)=>{
      console.log("response:",response.data);
    }).catch((error)=>{
      console.log("Error: ",error);
    })
    axios.get('https://deploy-mern-three.vercel.app/users').then((response)=>{
      console.log(response.data);
    }).catch((error)=>{
      console.log("Error: ",error);
    })
  },[])
  return (
    <div>App</div>
  )
}

export default App