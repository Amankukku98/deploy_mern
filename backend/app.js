const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const result = dotenv.config();
const router=require('./routes/router.js');
require('./db/connection.js');
if (result.error) {
  console.error('Error loading .env file:', result.error);
}
const app = express();
app.use(cors());
app.use(express.json());
app.use(router);
// type of port should be number otherwise it will give error
const PORT = parseInt(process.env.PORT) || 8001;

app.listen(PORT, () => {
    console.log("Server listening on port", PORT);
});
