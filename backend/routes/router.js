const express = require('express');
const router=new express.Router();
const controllers=require('../controllers/usersControllers');
//routes
router.post('/user/register',controllers.userPost);
router.get('/',(req,res)=>{
    res.send("Hello Express!")
});
module.exports =router;