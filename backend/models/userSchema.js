const mongoose=require('mongoose');
const userSchema=new mongoose.Schema({
    fname:{
        type:String,
        required:true,
        trime:true,
    },
    lname:{
        type:String,
        required:true,
        trime:true,
    },
    email:{
        type:String,
        required:true,
        unique:true,
    },
})
//model
const users=new mongoose.model("user",userSchema);
module.exports = users;