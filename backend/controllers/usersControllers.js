const users=require('../models/userSchema');
exports.userPost=async(req,res)=>{
console.log(req.body);
const {fname,lname,email}=req.body;
if(!fname || !lname || !email){
    res.status(401).json('All Inputs is required!')
}
try{
const peruser=await users.findOne({email:email});
if(peruser){
    res.status(401).json('This user already exist in our database!')
}else{
    const userData=new users({fname,lname,email})
    await userData.save();
    res.status(200).json(userData)
}
}catch(error){
    res.status(401).json(error);
    console.log("catch block error",error);
}
}